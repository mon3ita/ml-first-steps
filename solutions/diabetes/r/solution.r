library(dplyr, warn.conflicts = FALSE)

df <- read.csv("../dataset.csv")

X.train <- sample_frac(df, 0.7)
train.index <- as.numeric(rownames(X.train))
X.test <- df[-train.index,]

y.train <- t(X.train[, ncol(X.train)])
X.train <- X.train[, 1:ncol(X.train)-1]

y.test <- t(X.test[, ncol(X.test)])
X.test <- X.test[, 1:ncol(X.test) - 1]

find.distances <- function(row, dataset, outcome) {

    distances <- list()
    for(i in 1:nrow(dataset)) {
        distance <- sqrt(round(Reduce('+', lapply(dataset[i,] - t(row), "^", 2)), digits=2))
        values <- list(distance, outcome[[i]])
        distances <- append(distances, list(values))
    }

    return(distances)
}

count.class <- function(distance, class) {
    count <- 0

    for(i in 1:length(distance)) {
        if(distance[[i]][[2]] == class) {
            count <- count + 1
        }
    }

    return(count)
}

classify <- function(training, outcome, test, k) {
    distances <- list()

    for(row in test) {
        distances <- append(distances, list(find.distances(row, training, outcome)))
    }

    predicted <- list()

    for(distance in distances) {
        distance <- distance[order(sapply(distance, '[[', 1))][1:k]
        has_diabetes <- count.class(distance, 1)

        if(has_diabetes > floor(k / 2)) {
            predicted <- append(predicted, 1)
        } else {
            predicted <- append(predicted, 0)
        }
    }

    return(predicted)
}

predicted <- classify(X.train, y.train, X.test, 3)

accuracy <- function(predicted, actual) {
    err <- 0
    for(i in 1:length(predicted)) {
        if(predicted[[i]] != actual[[i]]) {
            err <- err + 1
        }
    }

    return(1 - (err / length(actual)))
}

print(paste("Accuracy: ", accuracy(predicted, y.test)))