require(stringr)
library(dplyr, warn.conflicts = FALSE)
library(tm)

df <- read.csv("./dataset.csv", sep=",")
df$email <- str_trim(df$email)

X.train <- sample_frac(df, 0.7)
train.index <- as.numeric(rownames(X.train))
X.test <- df[-train.index,]

y.train <- t(X.train[, ncol(X.train)])
X.train <- X.train[, 1:ncol(X.train)-1]

y.test <- t(X.test[, ncol(X.test)])
X.test <- X.test[, 1:ncol(X.test) - 1]

normalize <- function(email) {
    normalized <- tolower(gsub('[[:punct:] ]+', ' ', email))
    normalized <- gsub('\t', '', normalized)
    return(normalized)
}

vocabulary <- function(emails) {

    vocab <- list()
    for(email in emails) {
        normalized <- normalize(email)
        normalized <- removeWords(normalized, stopwords("en"))
        words <- strsplit(normalized, " ")
        vocab <- apply(cbind(vocab, words), 1, unlist)
        vocab <- unique(vocab)
    }

    return(unique(vocab))
}

transform <- function(row, vocab) {
    words <- rep(0, length(vocab))

    for(i in 1:length(vocab)) {
        for(j in 1:length(vocab[[i]])) {
            normalized <- normalize(row)
            normalized <- removeWords(normalized, stopwords("en"))
            row_words <- strsplit(normalized, ' ')
            if(vocab[[i]][j] %in% row_words) {
                words[i] <- 1
            }
        }
    }

    return(words)
}

fit <- function(X.train, y.tain, vocab) {
    spam <- rep(0, length(vocab))
    ham <- rep(0, length(vocab))

    for(i in 1:length(X.train)) {
        words <- transform(X.train[i], vocab)
        if(y.train[i] == 1) {
            spam <- lapply(seq_along(spam), function(i) unlist(spam[i]) + unlist(words[i]))
        } else {
            ham <- lapply(seq_along(ham), function(i) unlist(ham[i]) + unlist(words[i]))
        }
    }

    return(list("spam"=spam, "ham"=ham))
}

test <- function(X.test, y.test, y.train, spam, ham, vocab) {

    total_spam <- Reduce("+", y.train)
    total_ham <- length(y.train) - total_spam

    total_words <- length(vocab)

    predicted <- list
    for(email in X.test) {
        words <- transform(email, vocab)

        spam_score <- 0
        ham_score <- 0
        for(i in 1:length(words)) {
            if(words[i] == 0) {
                next
            }

            sp <- spam[i] / (total_spam + total_words)
            hm <- ham[i] / (total_ham + total_words)

            spam_score <- spam_score + sp
            ham_score <- ham_score + hm
        }

        if(spam_score > ham_score) {
            predicted <- append(predicted, 1)
        } else {
            predicted <- append(predicted, 0)
        }
    }

    return(predicted)
}

accuracy <- function(predicted, y.test) {
    err <- 0

    for(i in 1:length(predicted)) {
        if(predicted[i] != y.test[i]) {
            err <- err + 1
        }
    }

    total_err <- err / length(y.test)

    print(paste("Accuracy: ", 1 - total_err))
}

vocab <- vocabulary(X.train)

fitted <- fit(X.train, y.train, vocab)
spam <- fitted["spam"]
ham <- fitted["ham"]

predicted <- test(X.test, y.test, y.train, spam, ham, vocab)
accuracy(predicted, y.test)
