# ML

Contains various solutions of different problems.

## Problems

*Using `sklearn` and my implementation*

### [diabetes](solutions/diabetes) `sl`
### [drink consumption](solutions/drink_consumption) `sl`
### [graduate admission](solutions/graduate_admission) `sl`
### [mushroom classification](solutions/mushroom_classificaion) `sl`
### [spam email detection](solutions/spam_email_detection) `sl`
### [wine quality](solutions/wine_quality) `sl`
### [vacation for couples](solutions/vacation_for_couples) `usl`
### [frequent itemset](solutions/frequent_itemset) `usl`


*Using only `sklearn`*

### [hepatitis](solutions/hepatitis) `sl`
### [healthy or unhealthy](solutions/healthy_or_unhealthy) `sl`
### [cats vs. dogs](solutions/cats_vs_dogs) `sl`
### [cars](solutions/cars) `sl`